package com.cordova.plugins.push.baidu;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;

import com.google.gson.Gson;
import android.R;
import android.content.Context;
import java.io.PrintStream;
import java.io.OutputStream;

public class MyNotificationAnalytics {
    private static final String TAG = "Payke";

    public void register(Context context, String channelId, Map<String, Object> data) {
        Log.d(TAG, "Push_Notification_Receive_Event register. Start");
        int strId = context.getResources().getIdentifier("PAYKE_QUEUE_URI", "string", context.getPackageName());

        if (strId == 0 | !data.containsKey("token") || !data.containsKey("pushHistoryId")) {
            /* QueueStorageに蓄積するための情報が不足しているためスキップ */
            Log.d(TAG, "Push_Notification_Receive_Event register. Skip");
            return;
        }

        String queue_uri   = context.getString(strId);
        Object obj_queue_token = data.get("token");
        Object obj_push_history_id = data.get("pushHistoryId");

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("fcm_token", channelId);
        params.put("notification_history_id", obj_push_history_id.toString());
        params.put("application_state", "0"); // XXX: FCMのデータ受信を行うServiceクラスは、resume pauseイベントを取れないためバックグラウンドか判別不可
        Gson gson = new Gson();
        String json_str = gson.toJson(params); 
        String body_xml = "<QueueMessage><MessageText>" + json_str + "</MessageText></QueueMessage>";

        // HttpURLConnection connection = null;
        // String result = null;
        
        // try {
        //     URL url = new URL(queue_uri + "/messages?" + obj_queue_token.toString());
        //     connection = (HttpURLConnection) url.openConnection();
        //     connection.setReadTimeout(10000);
        //     connection.setConnectTimeout(20000);
        //     connection.setRequestMethod("POST");
        //     connection.setInstanceFollowRedirects(false);
        //     connection.setRequestProperty("Accept-Language", "jp");
        //     connection.addRequestProperty("Content-Type", "text/xml");
        //     connection.setDoOutput(true);
        //     connection.setDoInput(true);
        //     connection.connect();

        //     /* RequestBodyへの書き出し */
        //     try(OutputStream outputStream = connection.getOutputStream();
        //         PrintStream ps = new PrintStream(outputStream)) {
        //         ps.print(body_xml);
        //     }

        //     int resp = connection.getResponseCode();

        //     switch (resp){
        //         case HttpURLConnection.HTTP_OK:
        //         case HttpURLConnection.HTTP_CREATED:
        //             Log.i(TAG, "Push_Notification_Receive_Event register. result.status = HTTP_OK");
        //             break;
        //         case HttpURLConnection.HTTP_UNAUTHORIZED:
        //             Log.i(TAG, "Push_Notification_Receive_Event register. result.status = HTTP_UNAUTHORIZED");
        //             break;
        //         default:
        //             Log.i(TAG, "Push_Notification_Receive_Event register. result.status = HTTP_OTHER("+resp+")");
        //             break;
        //     }


        // } catch (Exception e) {

        //     StackTraceElement[] ste = e.getStackTrace();
        //     Log.i(TAG, "Push_Notification_Receive_Event register. Exception /" + e.getClass().getName() + ": "+ e.getMessage());
        //     Log.i(TAG, "Push_Notification_Receive_Event register. Exception /" + "\tat "+ ste[ste.length-1]);
        //     //Log.i(TAG, "Push_Notification_Receive_Event register. Exception error=" + e.getMessage());
        // } finally {
        //     Log.d(TAG, "Push_Notification_Receive_Event register. END");
        //     if (connection != null) {
        //         connection.disconnect();
        //     }
        // }


        AsyncTask<Void, Void, Void> queueStrageTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                HttpURLConnection connection = null;
                String result = null;
        
                try {
                    URL url = new URL(queue_uri + "/messages?" + obj_queue_token.toString());
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setReadTimeout(10000);
                    connection.setConnectTimeout(20000);
                    connection.setRequestMethod("POST");
                    connection.setInstanceFollowRedirects(false);
                    connection.setRequestProperty("Accept-Language", "jp");
                    connection.addRequestProperty("Content-Type", "text/xml");
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.connect();

                    /* RequestBodyへの書き出し */
                    try(OutputStream outputStream = connection.getOutputStream();
                        PrintStream ps = new PrintStream(outputStream)) {
                        ps.print(body_xml);
                    }
 
                    int resp = connection.getResponseCode();

                    switch (resp){
                        case HttpURLConnection.HTTP_OK:
                        case HttpURLConnection.HTTP_CREATED:
                            Log.i(TAG, "Push_Notification_Receive_Event register. result.status = HTTP_OK");
                            break;
                        case HttpURLConnection.HTTP_UNAUTHORIZED:
                            Log.i(TAG, "Push_Notification_Receive_Event register. result.status = HTTP_UNAUTHORIZED");
                            break;
                        default:
                            Log.i(TAG, "Push_Notification_Receive_Event register. result.status = HTTP_OTHER("+resp+")");
                            break;
                    }


                } catch (Exception e) {

                    StackTraceElement[] ste = e.getStackTrace();
                    Log.i(TAG, "Push_Notification_Receive_Event register. Exception /" + e.getClass().getName() + ": "+ e.getMessage());
                    Log.i(TAG, "Push_Notification_Receive_Event register. Exception /" + "\tat "+ ste[ste.length-1]);
                    //Log.i(TAG, "Push_Notification_Receive_Event register. Exception error=" + e.getMessage());
                } finally {
                    Log.d(TAG, "Push_Notification_Receive_Event register. END");
                    if (connection != null) {
                        connection.disconnect();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

            }
        };

        queueStrageTask.execute();

        return;
    }
}
