package com.cordova.plugins.push.baidu;

import java.util.ArrayList;
import java.util.List;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import android.util.Log;

import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.android.pushservice.CustomPushNotificationBuilder;

import android.app.Notification;
import android.content.res.Resources;
import android.graphics.BitmapFactory;

import android.net.Uri;
import android.provider.MediaStore;
import android.os.Build;
import android.app.NotificationManager;
import android.content.Context;
import android.app.NotificationChannel;



/**
 * 百度云推送插件
 * 
 * @author NCIT
 *
 */
public class BaiduPush extends CordovaPlugin {
    /** LOG TAG */
    private static final String LOG_TAG = BaiduPush.class.getSimpleName();

	/** JS回调接口对象 */
    public static CallbackContext pushCallbackContext = null;
    public static CallbackContext onMessageCallbackContext = null;
    public static CallbackContext onNotificationClickedCallbackContext = null;
    public static CallbackContext onNotificationArrivedCallbackContext = null;

    /**
     * 插件初始化
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    	Log.d(LOG_TAG, "xxxxxxBaiduPush#initialize");

        super.initialize(cordova, webView);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = (NotificationManager)cordova.getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        
            NotificationChannel channel = new NotificationChannel(
                // 一意のチャンネルID
                // ここはどこかで定数にしておくのが良さそう
                "channel_id_sample",
        
                // 設定に表示されるチャンネル名
                // ここは実際にはリソースを指定するのが良さそう
                "プッシュ通知",
        
                // チャンネルの重要度
                // 重要度によって表示箇所が異なる
                NotificationManager.IMPORTANCE_DEFAULT
            );
            // ロック画面での表示レベル
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        
            // チャンネルの登録
            manager.createNotificationChannel(channel);
        }        
    }

    /**
     * 插件主入口
     */
    @Override
    public boolean execute(String action, final JSONArray args, CallbackContext callbackContext) throws JSONException {
    	Log.d(LOG_TAG, "xxxxxxBaiduPush#execute");

    	boolean ret = false;
        
        if ("startWork".equalsIgnoreCase(action)) {

            Log.d(LOG_TAG,"xxxx####startWork start");

            pushCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            
            final String apiKey = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	Log.d(LOG_TAG, "PushManager#startWork");
                    PushManager.startWork(cordova.getActivity().getApplicationContext(),
                            PushConstants.LOGIN_TYPE_API_KEY, apiKey);

                    String pkgname = cordova.getActivity().getPackageName();
                    Resources res = cordova.getActivity().getResources();

                    CustomPushNotificationBuilder cBuilder = new CustomPushNotificationBuilder(
                        res.getIdentifier("notification_custom_builder", "layout", pkgname),
                        res.getIdentifier("notification_icon", "id", pkgname),
                        res.getIdentifier("notification_title", "id", pkgname),
                        res.getIdentifier("notification_text", "id", pkgname)
                    );

                    cBuilder.setNotificationFlags(Notification.FLAG_AUTO_CANCEL);
                    cBuilder.setNotificationDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    cBuilder.setNotificationSound(Uri.withAppendedPath( MediaStore.Audio.Media.INTERNAL_CONTENT_URI, "6").toString());
                    cBuilder.setStatusbarIcon(res.getIdentifier("fcm_push_icon", "mipmap", pkgname));
                    cBuilder.setLayoutDrawable(res.getIdentifier("fcm_push_icon", "mipmap", pkgname));
    
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        cBuilder.setChannelId("channel_id_sample");
                    }

                    PushManager.setNotificationBuilder(cordova.getActivity(), 1, cBuilder);
                }
            });
            ret =  true;
        } else if ("stopWork".equalsIgnoreCase(action)) {
            pushCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	Log.d(LOG_TAG, "xxxxxPushManager#stopWork");
                    PushManager.stopWork(cordova.getActivity().getApplicationContext());
                }
            });
            ret =  true;
        } else if ("resumeWork".equalsIgnoreCase(action)) {
            pushCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	Log.d(LOG_TAG, "xxxxPushManager#resumeWork");
                    PushManager.resumeWork(cordova.getActivity().getApplicationContext());
                }
            });
            ret = true;
        } else if ("setTags".equalsIgnoreCase(action)) {

            Log.d(LOG_TAG,"xxxxx####setTags start");

            pushCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	Log.d(LOG_TAG, "xxxxxPushManager#setTags");
                	
                	List<String> tags = null;
                	if (args != null && args.length() > 0) {
                		int len = args.length();
                		tags = new ArrayList<String>(len);
                		
                		for (int inx = 0; inx < len; inx++) {
                			try {
								tags.add(args.getString(inx));
							} catch (JSONException e) {
								LOG.e(LOG_TAG, e.getMessage(), e);
							}
                		}

                		PushManager.setTags(cordova.getActivity().getApplicationContext(), tags);
                	}
                	
                }
            });
            ret = true;
        } else if ("delTags".equalsIgnoreCase(action)) {
            pushCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	Log.d(LOG_TAG, "xxxxxxPushManager#delTags");
                	
                	List<String> tags = null;
                	if (args != null && args.length() > 0) {
                		int len = args.length();
                		tags = new ArrayList<String>(len);
                		
                		for (int inx = 0; inx < len; inx++) {
                			try {
								tags.add(args.getString(inx));
							} catch (JSONException e) {
								LOG.e(LOG_TAG, e.getMessage(), e);
							}
                		}

                		PushManager.delTags(cordova.getActivity().getApplicationContext(), tags);
                	}
                	
                }
            });
            ret = true;
        } else if ("onMessage".equalsIgnoreCase(action)) {

            Log.d(LOG_TAG,"xxxx####onMessage start");

            onMessageCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            ret = true;
        } else if ("onNotificationClicked".equalsIgnoreCase(action)) {

            Log.d(LOG_TAG,"xxxx####onNotificationClicked start");

            onNotificationClickedCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            ret = true;

            //kill->起動時にcallback登録より前にreceiveでイベントが起きてる場合、ちゃんと動かす
            BaiduPushReceiver.baidupushreceiver.sendQueueOnNotificationClick();

        } else if ("onNotificationArrived".equalsIgnoreCase(action)) {

            Log.d(LOG_TAG,"xxxx####onNotificationArrived start");

            onNotificationArrivedCallbackContext = callbackContext;
            
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            ret = true;
        }

        return ret;
    }
}
