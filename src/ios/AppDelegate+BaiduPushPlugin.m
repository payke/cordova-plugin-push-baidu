#import "AppDelegate+BaiduPushPlugin.h"
#import "BaiduPushPlugin.h"
#import "BPush.h"
#import <objc/runtime.h>


// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
typedef void (^CompletionHandlerType)();

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif


@implementation AppDelegate (BaiduPushPlugin) 

static NSString *notificationCallback = @"var callbackNotificationCallBack = function(token){ if (typeof FCMPlugin != 'undefined') {FCMPlugin.onNotificationReceived(token)}else{console.log(token)}}; callbackNotificationCallBack";
static NSString *tokenRefreshCallback = @"var callbackTokenRefreshCallBack = function(token){ if (typeof FCMPlugin != 'undefined') {FCMPlugin.onTokenRefreshReceived(token)}else{console.log(token)}}; callbackTokenRefreshCallBack";


+ (void) load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self baiduPushSwizzle: @selector(application:didFinishLaunchingWithOptions:)
                          with: @selector(application:baiduDidFinishLaunchingWithOptions:)];
    });
}
    
+ (void) baiduPushSwizzle:(SEL)originalSelector with:(SEL)swizzledSelector {
    Class class = [self class];
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    BOOL success = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    if (success) {
        class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}
    
- (BOOL) application:(UIApplication *)application baiduDidFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    NSLog(@"xxxxxpushが届いたかわはわからない[didFinishLaunchingWithOptions]");

    
    //まず先にやってあげる
    [self application:application baiduDidFinishLaunchingWithOptions:launchOptions];
    
    [BaiduPushPlugin setLaunchOptions:launchOptions];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
        
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    return YES;
}

- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [BPush registerDeviceToken:deviceToken];
}

- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"xxxxRegister remote notification failed: %@", error);
}

    
// [START receive_message in background iOS < 10]

// Include the iOS < 10 methods for handling notifications for when running on iOS < 10.
// As in, even if you compile with iOS 10 SDK, when running on iOS 9 the only way to get
// notifications is the didReceiveRemoteNotification.
    
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{

    // Short-circuit when actually running iOS 10+, let notification centre methods handle the notification.
    if (NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_9_x_Max) {
        return;
    }
    
    NSDictionary *userInfoMutable = [userInfo mutableCopy];
    
    if (application.applicationState != UIApplicationStateActive) {
        [userInfoMutable setValue:@(YES) forKey:@"wasTapped"];
    }
    
}
// [END receive_message in background] iOS < 10]


- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {

    NSDictionary *userInfoMutable = [userInfo mutableCopy];

    if(application.applicationState == UIApplicationStateInactive){
        //龍メモ
        //firebaseにあわせてwasTapped
        [userInfoMutable setValue:@(YES) forKey:@"wasTapped"];
    }
    
    [BaiduPushPlugin receiveNotification:userInfoMutable
                            inBackground:([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive)];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    @try {
        [self paykeReceiveNotificationEventRegisterWithUserInfo:userInfoMutable];
    }
    @catch (NSException *e){
        NSLog(@"xxxxx[Payke]ReceiveNotificationEvent faile. reason = %@",e);
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}
    
    
    
    // [START message_handling]
    // Receive displayed notifications for iOS 10 devices.
    
    // Note on the pragma: When compiling with iOS 10 SDK, include methods that
    //                     handle notifications using notification center.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    
    // Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;

    NSDictionary *userInfoMutable = [userInfo mutableCopy];
    
    //龍メモ
    //firebaseにあわせてwasTapped
    [userInfoMutable setValue:@(YES) forKey:@"wasTapped"];
    
    [BaiduPushPlugin receiveNotification:userInfoMutable
                            inBackground:([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive)];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    @try {
        [self paykeReceiveNotificationEventRegisterWithUserInfo:userInfoMutable];
    }
    @catch (NSException *e){
        NSLog(@"xxxxxx[Payke]ReceiveNotificationEvent faile. reason = %@",e);
    }
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}
    
    // Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    NSDictionary *userInfoMutable = [userInfo mutableCopy];
    
    //龍メモ
    //firebaseにあわせてwasTapped
    [userInfoMutable setValue:@(YES) forKey:@"wasTapped"];
    
    [BaiduPushPlugin receiveNotification:userInfoMutable
                            inBackground:([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive)];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    @try {
        [self paykeReceiveNotificationEventRegisterWithUserInfo:userInfoMutable];
    }
    @catch (NSException *e){
        NSLog(@"xxxxxx[Payke]ReceiveNotificationEvent faile. reason = %@",e);
    }

    
    completionHandler();
}
#endif
    


- (void)paykeReceiveNotificationEventRegisterWithUserInfo:(NSDictionary *)userInfo
{
    NSLog(@"[Payke]Push_Notification_Receive_Event register. Start ");
    NSString *queue_token     = [userInfo valueForKey:@"token"];
    NSString *push_history_id = [userInfo valueForKey:@"pushHistoryId"];
    NSString *queue_uri = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PAYKE_QUEUE_URI"];
    if (queue_token == nil || push_history_id == nil || queue_uri == nil) {
        NSLog(@"[Payke]Push_Notification_Receive_Event register. Skip");
        return;
    }

    NSLog(@"[Payke]Push_Notification_Receive_Event register. create request");
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/messages?%@", queue_uri, queue_token]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"text/xml" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:[self getToQueueMessageXmlDataWithPushId:push_history_id fcm_token:[BPush getChannelId]]];
    NSLog(@"[Payke]Push_Notification_Receive_Event register. create request comp.");
    
    NSLog(@"[Payke]Push_Notification_Receive_Event register. create session");
    NSURLSessionConfiguration *sessionConfig;
    sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:push_history_id];
    sessionConfig.allowsCellularAccess = YES;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                          delegate:self
                                                     delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request];
    NSLog(@"[Payke]Push_Notification_Receive_Event register. session resume");
    [task resume];
    NSLog(@"[Payke]Push_Notification_Receive_Event register. End");

    return;
}


- (NSData *)getToQueueMessageXmlDataWithPushId:(NSString *)push_history_id fcm_token:(NSString *)baidu_channel
{
    /* QueueStorageのメッセージとして格納する受信したPush通知に関する情報を生成 */
    
    //龍メモ
    //運用的に、baiduの時はbaidu向けに鹿送らない。基本混ざることはない（by重岡）
    //なので、push送信数だけをここで記して、baiduとfirebaseの数はpush_history_idから判別するに止まる
    
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    NSDictionary     *params = @{
                                 @"fcm_token": baidu_channel,
                                 @"notification_history_id": push_history_id,
                                 @"application_state": state == UIApplicationStateBackground ? @"1":@"2"
                                 };
    NSError  *error    = nil;
    NSData   *data     = [NSJSONSerialization dataWithJSONObject:params options:2 error:&error];
    NSString *json_str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSString *body_xml = [NSString stringWithFormat:@"<QueueMessage><MessageText>%@</MessageText></QueueMessage>", json_str];
    return [body_xml dataUsingEncoding:NSUTF8StringEncoding];
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    [self addCompletionHandler:completionHandler forSession:identifier];
}


#pragma mark - NSURLSessionDelegate
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    if (session.configuration.identifier) {
        [self callCompletionHandlerForSession:session.configuration.identifier];
    }
}

#pragma mark - Private methods
- (void)addCompletionHandler:(CompletionHandlerType)handler forSession:(NSString *)identifier
{
    self.completionHandlerDictionary[identifier] = handler;
}

- (void)callCompletionHandlerForSession: (NSString *)identifier
{
    CompletionHandlerType handler = self.completionHandlerDictionary[identifier];
    if (handler) {
        [self.completionHandlerDictionary removeObjectForKey:identifier];
        handler();
    }
}

@end
