//
//  BaiduPushPlugin.h
//  HaobanPlugin
//
//  Created by 马杰磊 on 15/6/17.
//  Copyright (c) 2015年 NTTData. All rights reserved.
//

#ifndef _____BaiduPushPlugin_h
#define _____BaiduPushPlugin_h

#import <Cordova/CDV.h>
#import <CoreLocation/CoreLocation.h>
#import "BPush.h"

static NSDictionary *launchOptions = nil;
static BOOL setFlag = false;
static CDVInvokedUrlCommand *clickCallback = nil;
static CDVInvokedUrlCommand *arriveCallback = nil;
static CDVPluginResult *pluginResult = nil;
static id delegate = nil;

@interface BaiduPushPlugin : CDVPlugin

+ (void)setLaunchOptions:(NSDictionary*)options;

+ (void)receiveNotification:(NSDictionary *)userInfo inBackground:(BOOL) background;

- (void)startWork:(CDVInvokedUrlCommand*)command;

- (void)stopWork:(CDVInvokedUrlCommand*)command;

- (void)resumeWork:(CDVInvokedUrlCommand*)command;

- (void)setTags:(CDVInvokedUrlCommand*)command;

- (void)delTags:(CDVInvokedUrlCommand*)command;

- (void)listTags:(CDVInvokedUrlCommand*)command;

- (void)onNotificationClicked:(CDVInvokedUrlCommand*)command;

- (void)onNotificationArrived:(CDVInvokedUrlCommand*)command;

@end

#endif
