#import "AppDelegate.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif


#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0

@interface AppDelegate (BaiduPushPlugin) <UNUserNotificationCenterDelegate, NSURLSessionDelegate>
@property NSMutableDictionary *completionHandlerDictionary;
@end

#else

@interface AppDelegate (BaiduPushPlugin) <NSURLSessionDelegate>
@property NSMutableDictionary *completionHandlerDictionary;
@end
#endif

