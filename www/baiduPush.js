var exec = require('cordova/exec');

var baiduPushPluginResult;
var baiduPushPluginChannelId = null;
var baiduPushPluginUserId = null;

var baiduPush = {
  onMessage: function(successCallback, failureCallback){
      exec(successCallback, failureCallback, 'BaiduPush', 'onMessage', []);
  },
  onNotificationClicked: function(successCallback, failureCallback){
      exec(successCallback, failureCallback, 'BaiduPush', 'onNotificationClicked', []);
  },
  onNotificationArrived: function(successCallback, failureCallback){
      exec(successCallback, failureCallback, 'BaiduPush', 'onNotificationArrived', []);
  },
  startWork: function(api_key, successCallback, failureCallback) {

    //getToken的なものを作ってみる
    c = (result) => {
      baiduPushPluginResult = result.data;
      baiduPushPluginChannelId = result.data.channelId;
      baiduPushPluginUserId = result.data.userId;

      successCallback(result);
    }

    exec(c, failureCallback, 'BaiduPush', 'startWork', [api_key]);
  },
  stopWork: function(successCallback, failureCallback) {
    exec(successCallback, failureCallback, 'BaiduPush', 'stopWork', []);
  },
  resumeWork: function(successCallback, failureCallback) {
    exec(successCallback, failureCallback, 'BaiduPush', 'resumeWork');
  },
  setTags: function(tags) {
    return new Promise((resolve, reject) => {
      exec(resolve, reject, 'BaiduPush', 'setTags', tags);
    });
    // exec(successCallback, failureCallback, 'BaiduPush', 'setTags', tags);
  },
  delTags: function(tags) {
    return new Promise((resolve, reject) => {
      exec(resolve, reject, 'BaiduPush', 'delTags', tags);
    });
    // exec(successCallback, failureCallback, 'BaiduPush', 'delTags', tags);
  },
  // setTags: function(tags) {
  //   exec(
  //     (r)=>{console.log("success setTags tag[" + tags + "] to baiduPush");},
  //     (e)=>{console.error("fail setTags tag[" + tags + "] to baiduPush. error="+e);},
  //     'BaiduPush', 
  //     'setTags', 
  //     tags);
  // },
  // delTags: function(tags) {
  //   exec(
  //     (r)=>{console.log("success delTags tag[" + tags + "] to baiduPush");},
  //     (e)=>{console.error("fail delTags tag[" + tags + "] to baiduPush. error="+e);},
  //     'BaiduPush', 
  //     'delTags', 
  //     tags);
  // },
  
  // getPushConnectionResult: function() {
  //   return baiduPushPluginResult;
  // }

  getPushConnectionResult: function() {
    return baiduPushPluginResult;
  }

}

module.exports = baiduPush;